<?php
/**
 * Push Function.
 *
 * PHP version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 * @date    August 2018
 */

namespace Squiz\AsyncIO;


/**
 * Identity.
 *
 * @param mixed $value A value to return.
 *
 * @return mixed Returns the same value.
 */
function identity($value)
{
    return $value;

}//end identity()
