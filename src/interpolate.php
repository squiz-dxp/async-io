<?php
/**
 * Filter not null.
 *
 * PHP version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 * @date    August 2018
 */
namespace Squiz\AsyncIO;


/**
 * Array Prefix.
 *
 * Prefix each string in the given array with a given string.
 *
 * @param string $prefix The prefix to use.
 * @param array  $arr    The array of strings to add the prefix to.
 *
 * @return array
 */
function arrayPrefix($prefix, array $arr)
{
    if (!is_array($arr)) {
        return false;
    }

    return array_map(
        function ($val) use ($prefix) {
            return $prefix.$val;
        },
        $arr
    );

}//end arrayPrefix()


/**
 * Array Prefix Keys.
 *
 * @param string $prefix The prefix to use.
 * @param array  $arr    The array with keys to add the prefix to.
 *
 * @return array
 */
function arrayPrefixKeys($prefix, array $arr)
{
    $keys = arrayPrefix($prefix, array_keys($arr));
    return array_combine($keys, array_values($arr));

}//end arrayPrefixKeys()


/**
 * Interpolate.
 *
 * @param string $template The template to place data into.
 *
 * @return callable Returns a function that will interpolate 'variables' into the template.
 */
function interpolate($template)
{
    return function ($data) use ($template) {
        $conversions = arrayPrefixKeys('$', $data);
        return strtr($template, $conversions);
    };

}//end interpolate()
