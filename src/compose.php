<?php
/**
 * Compose function.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO;


/**
 * Reducer.
 *
 * @param mixed $result The accumulator variable.
 * @param mixed $item   The item of the array to process.
 *
 * @return mixed
 */
function reducer($result, $item)
{
    return !is_array($result) ? call_user_func_array($item, [$result]) : call_user_func_array($item, $result);

}//end reducer()


/**
 * Compose.
 *
 * A multivariate compose() implementation for PHP. Based on
 * https://medium.com/@assertchris/function-composition-c8094ae9be63
 *
 * @return callable
 */
function compose()
{
    $callbacks = func_get_args();

    return function () use ($callbacks) {
        $arguments = func_get_args();

        return array_reduce($callbacks, __NAMESPACE__.'\reducer', $arguments);
    };

}//end compose()


/**
 * Compose Two Functions
 *
 * @param callable $f The first function to be composed.
 * @param callable $g The second function to be composed.
 *
 * @return callable
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
function compose2(callable $f, callable $g)
{
    return function ($x) use ($f, $g) {
        return $f($g($x));
    };

}//end compose2()
