<?php
/**
 * Kestrel utility function.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO;


/**
 * Kestrel Combinator.
 *
 * Also known as `functionify`. Creates a function that will always return the
 * same value, no matter what it is passed. This is handy for turning a
 * value into a callback, for example.
 *
 * @param mixed $val The value to return.
 *
 * @return callable
 *
 * @SuppressWarnings(PHPMD.CamelCaseMethodName)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
function K($val) // phpcs:ignore Squiz.NamingConventions.ValidFunctionName.NotCamelCaps
{
    return function () use ($val) {
        return $val;
    };

}//end K()
