<?php
/**
 * Task Class.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO;

use Squiz\AsyncIO\Proc;
use Squiz\AsyncIO\Async;


/**
 * Task.
 */
class Task
{

    /**
     * Fork Function.
     *
     * @var callable
     */
    private $forkFunc;


    /**
     * Constructor.
     *
     * @param callable $resolver The function to run the task.
     *
     * @return void
     */
    public function __construct(callable $resolver)
    {
        $this->forkFunc = $resolver;

    }//end __construct()


    /**
     * Of.
     *
     * @param mixed $val The value ot pass into a task.
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @return Task
     */
    public static function of($val)
    {
        return new Task(
            function ($reject, $resolve) use ($val) {
                try {
                    return $resolve($val);
                } catch (\Exception $e) {
                    return $reject($e);
                }
            }
        );

    }//end of()


    /**
     * Reject.
     *
     * @param \Exception $err The exception to reject a task with.
     *
     * @return Task
     */
    public static function reject(\Exception $err)
    {
        return new Task(function ($reject) use ($err) {
            return $reject($err);
        });

    }//end reject()


    /**
     * Map.
     *
     * @param callable $func The function to map.
     *
     * @return mixed
     */
    public function map(callable $func)
    {
        return new Task(
            function ($reject, $resolve) use ($func) {
                return $this->fork($reject, compose2($resolve, $func));
            }
        );

    }//end map()


    /**
     * Chain.
     *
     * @param callable $func A function that returns a new task.
     *
     * @return Task
     */
    public function chain(callable $func)
    {
        return new Task(
            function ($reject, $resolve) use ($func) {
                return $this->fork(
                    $reject,
                    function ($x) use ($func, $reject, $resolve) {
                        $chainResult = $func($x);
                        if (!method_exists($chainResult, 'fork')) {
                            $msg = "Chained function returned something that isn’t a Task:\n";
                            $dat = var_export($chainResult, true);
                            $err = new \RuntimeException($msg.$dat);
                            return Task::reject($err)->fork($reject, $resolve);
                        }

                        return $chainResult->fork($reject, $resolve);
                    }
                );
            }
        );

    }//end chain()


    /**
     * Join.
     *
     * @return Task
     */
    public function join()
    {
        return $this->chain(__NAMESPACE__.'\identity');

    }//end join()


    /**
     * Ap.
     *
     * @param Task $taskContainingFunc A task containing a callable function.
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @return Task
     */
    public function ap(Task $taskContainingFunc)
    {
        return $taskContainingFunc->chain(
            function ($func) {
                return $this->map($func);
            }
        );

    }//end ap()


    /**
     * Fork.
     *
     * @param callable $reject  Function to call if task fails.
     * @param callable $resolve Function to call if task succeeds.
     *
     * @return mixed
     */
    public function fork(callable $reject, callable $resolve)
    {
        return call_user_func($this->forkFunc, $reject, $resolve);

    }//end fork()


    /**
     * Filter Tasks.
     *
     * @param callable $predicate The predicate to filter by.
     * @param array    $values    An array of tasks to filter.
     *
     * @return Task    A task containing and array of filtered values.
     */
    public static function filterByTaskPredicate(callable $predicate, array $values)
    {
        return Async::parallel(array_map($predicate, $values))->map(function ($bools) use ($values) {
            return array_values(array_intersect_key($values, array_filter($bools)));
        });

    }//end filterByTaskPredicate()


}//end class
