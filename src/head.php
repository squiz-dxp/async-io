<?php
/**
 * Head utility function.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO;


/**
 * Return the first item in an array.
 *
 * @param array $arr The array to pull an item out ot.
 *
 * @return mixed
 */
function head(array $arr)
{
    if (!is_array($arr) || (count($arr) <= 0)) {
        return null;
    }

    return $arr[0];

}//end head()
