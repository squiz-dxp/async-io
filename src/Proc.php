<?php
/**
 * Proc Function. Like proc_open() but with callbacks.
 *
 * PHP version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 * @date    July 2018
 */
namespace Squiz\AsyncIO;


/**
 * Proc Runner.
 *
 * Unfortunately, making this work requires having a global variable to store
 * processes that are still running. We encapsulate this in the $procs static
 * property.
 *
 * @SuppressWarnings(PHPMD.CamelCaseMethodName)
 */
class Proc
{

    /**
     * Array of procedures to be run.
     *
     * @var array
     */
    private static $procs = [];


    /**
     * Proc.
     *
     * @param array    $cmd     The command to run.
     * @param callable $reject  The callback to run if the command fails.
     * @param callable $resolve The callback to run if the command succeeds.
     *
     * @throws \RuntimeException Throws exception if command can't be run.
     *
     * @return void
     */
    public static function exec(array $cmd, callable $reject, callable $resolve)
    {
        $descriptorSpec = array(
            0 => array('pipe', 'r'),
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w'),
        );

        $command = implode(' ', array_map('escapeshellarg', $cmd));
        $process = proc_open($command, $descriptorSpec, $pipes);

        if (($process === false) || !is_resource($process)) {
            throw new \RuntimeException("Unable to execute command:\n".$cmd);
        }

        // Put pipes into non-blocking mode.
        stream_set_blocking($pipes[1], 0);
        stream_set_blocking($pipes[2], 0);

        // Close stdin so nobody can do anything silly.
        fclose($pipes[0]);

        self::$procs[] = array(
            'pipes'    => $pipes,
            'process'  => $process,
            'reject'   => $reject,
            'resolve'  => $resolve,
            'cmd'      => $command,
            'stderr'   => '',
            'stdout'   => '',
        );

    }//end exec()


    /**
     * Write to a file.
     *
     * @param string   $fname   The file to write to.
     * @param string   $data    The data to write to the file.
     * @param callable $reject  A callback to call if something fails.
     * @param callable $resolve A callback to call once the file is written.
     *
     * @throws \RuntimeException Throws exception if command can't be run.
     *
     * @return void
     */
    public static function write($fname, $data, callable $reject, callable $resolve)
    {
        $descriptorSpec = array(
            0 => array('pipe', 'r'),
            1 => array('file', $fname, 'w'),
            2 => array('pipe', 'w'),
        );

        $command = 'cat';
        $process = proc_open($command, $descriptorSpec, $pipes);

        if (($process === false) || !is_resource($process)) {
            throw new \RuntimeException("Unable to write to file: '".$fname."'\nUsing command: '".$command."'");
        }

        // Put pipes into non-blocking mode.
        stream_set_blocking($pipes[2], 0);

        // Write the data to the process. Then close stdin so nobody can do anything silly.
        fwrite($pipes[0], $data);
        fclose($pipes[0]);

        self::$procs[] = array(
            'pipes'    => $pipes,
            'process'  => $process,
            'reject'   => $reject,
            'resolve'  => $resolve,
            'cmd'      => $command,
            'stderr'   => '',
            'stdout'   => '',
        );

    }//end write()


    /**
     * Reject Failed Process.
     *
     * @param callable $reject    A callback to run on failure.
     * @param string   $cmd       The callback that caused the failure.
     * @param string   $stderr    The output from stderr.
     * @param string   $stdout    The output from stdout.
     * @param integer  $returnVal The return value from the process.
     *
     * @return mixed Returns whatever $reject() returns.
     */
    private static function _rejectFailedProc(callable $reject, $cmd, $stderr, $stdout, $returnVal)
    {
        $msg = join(
            "\n",
            [
                'Could not run command:',
                $cmd,
                'Return value: '.$returnVal,
                'Error: '.$stderr,
                'Stdout: '.$stdout,
            ]
        );
        $err = new \RuntimeException($msg);
        return $reject($err);

    }//end _rejectFailedProc()


    /**
     * Read and close.
     *
     * @param array $proc The process object to finish off.
     *
     * @return array
     */
    private static function _readAndClose(array $proc)
    {
        if (array_key_exists(1, $proc['pipes'])) {
            $stdoutPipe      = $proc['pipes'][1];
            $proc['stdout'] .= stream_get_contents($stdoutPipe);
            fclose($stdoutPipe);
        }

        $stderrPipe = $proc['pipes'][2];

        // Read STDERR from process.
        $proc['stderr'] .= stream_get_contents($stderrPipe);
        fclose($stderrPipe);

        return $proc;

    }//end _readAndClose()


    /**
     * Read and continue.
     *
     * @param array $proc The process object to read data for.
     *
     * @return array
     */
    private static function _readAndContinue(array $proc)
    {
        $stdoutPipe = (array_key_exists(1, $proc['pipes'])) ? $proc['pipes'][1] : null;
        $stderrPipe = $proc['pipes'][2];

        if ($stdoutPipe !== null) {
            $proc['stdout'] .= fgets($stdoutPipe, 512);
        }

        $proc['stderr'] .= fgets($stderrPipe, 512);
        return $proc;

    }//end _readAndContinue()


    /**
     * Check a running process.
     *
     * Check to see if it's still running and/or still had data to return.
     *
     * @param array $remaining An array of remaining processes (that have already been checked).
     * @param array $proc      An associative array of process data to check.
     *
     * @return array Returns an array of process data.
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private static function _checkProc(array $remaining, array $proc)
    {
        $procStatus       = proc_get_status($proc['process']);
        $stdout           = (array_key_exists(1, $proc['pipes'])) ? $proc['pipes'][1] : null;
        $stderr           = $proc['pipes'][2];
        $procStillRunning = (
            (!feof($stderr) || (($stdout !== null) && !feof($stdout)))
            && ($procStatus['running'] === true)
        );
        if ($procStillRunning) {
            return array_merge($remaining, [self::_readAndContinue($proc)]);
        }

        // If we've reached here then the process has closed both streams or stopped running entirely.
        // So, we want to finish it off, close all the pipes, and run callbacks.
        $proc = self::_readAndClose($proc);

        // Close the process and get the return value.
        $closeRetVal = proc_close($proc['process']);
        $returnVal   = ($procStatus['running']) ? $closeRetVal : $procStatus['exitcode'];

        if ($returnVal !== 0) {
            self::_rejectFailedProc($proc['reject'], $proc['cmd'], $proc['stderr'], $proc['stdout'], $returnVal);
            return $remaining;
        }

        $resolve = $proc['resolve'];
        $resolve([$proc['stdout'], $proc['stderr']]);
        return $remaining;

    }//end _checkProc()


    /**
     * Event Loop.
     *
     * @return void
     */
    public static function runEventLoop()
    {
        $numProcs = count(self::$procs);
        while ($numProcs > 0) {
            $proc        = array_shift(self::$procs);
            $newProcs    = self::_checkProc([], $proc);
            self::$procs = array_merge(self::$procs, $newProcs);

            $numProcs = count(self::$procs);
            if ($numProcs > 0) {
                usleep(1);
            }
        }//end while

    }//end runEventLoop()


}//end class

// Register shutdown function to complete the event loop.
register_shutdown_function(__NAMESPACE__.'\Proc::runEventLoop');
