<?php
/**
 * Push Function.
 *
 * PHP version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 * @date    August 2018
 */

namespace Squiz\AsyncIO;


/**
 * Curried array_push().
 *
 * @param array $arr An array to stick a new value into.
 *
 * @return callable Returns a function that will append a value to the array.
 */
function push(array $arr)
{
    return function ($val) use ($arr) {
        return array_merge($arr, [$val]);
    };

}//end push()
