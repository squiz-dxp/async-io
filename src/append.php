<?php
/**
 * Append Function.
 *
 * PHP version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 * @date    August 2018
 */

namespace Squiz\AsyncIO;


/**
 * Curried Append function.
 *
 * @param string $prefix The prefix to add on the end of a string.
 *
 * @return callable Returns a function that will add the prefix onto a string.
 */
function append($prefix)
{
    return function ($suffix) use ($prefix) {
        return $prefix.$suffix;
    };

}//end append()
