<?php
/**
 * Partial Application function.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO;


/**
 * Partially apply a variables to a function.
 *
 * This particular version based on
 * https://eddmann.com/posts/using-partial-application-in-php/
 *
 * @return callable
 */
function partial()
{
    $args = func_get_args();
    $func = array_shift($args);

    return function () use ($func, $args) {
        return call_user_func_array($func, array_merge($args, func_get_args()));
    };

}//end partial()
