<?php
/**
 * Async File System.
 *
 * Non-blocking IO functions for PHP. Hacked together using proc_open() and
 * register_shutdown_function(). If you're on a more recent version of PHP
 * than 5.4, I recommend using something like https://amphp.org/
 *
 * PHP version 5.4+
 *
 * @package Squiz\AsyncIO
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO;

use Squiz\AsyncIO\Proc;
use Squiz\AsyncIO\Task;

/**
 * Async.
 */
class Async
{


    /**
     * Proc if Null.
     *
     * Helper function for setting the default proc function.
     *
     * @param callable $proc The proc() function to use.
     *
     * @return callable The function to use for running the task.
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    private static function _procIfNull(callable $proc=null)
    {
        $exec = function ($cmd, $reject, $resolve) {
            return Proc::exec($cmd, $reject, $resolve);
        };

        return ($proc === null) ? $exec : $proc;

    }//end _procIfNull()


    /**
     * Asynchronous exec.
     *
     * @param array    $cmd  The command to run.
     * @param callable $proc The function to use to run the commant. Defaults to proc(). For testing only.
     *
     * @return Task Returns a Task monad.
     */
    public static function exec(array $cmd, callable $proc=null)
    {
        $proc = self::_procIfNull($proc);
        return new Task(
            function ($reject, $resolve) use ($proc, $cmd) {
                return $proc($cmd, $reject, $resolve);
            }
        );

    }//end exec()


    /**
     * Asynchronous Read File.
     *
     * @param string   $fname The name of the file to read.
     * @param callable $proc  The function to use to read the file. Defaults to proc(). For testing only.
     *
     * @return Task Returns a Task monad.
     */
    public static function readFile($fname, callable $proc=null)
    {
        return self::exec(['cat', $fname], $proc)->map(__NAMESPACE__.'\head');

    }//end readFile()


    /**
     * Write file.
     *
     * @param string $fname The file to write to.
     * @param string $data  The data to write to the file.
     *
     * @return Task
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function writeFile($fname, $data)
    {
        return new Task(function ($reject, $resolve) use ($fname, $data) {
            Proc::write($fname, $data, $reject, $resolve);
        });

    }//end writeFile()


    /**
     * File Exists.
     *
     * Test if a file exists.
     *
     * @param string $fname The file name to test the existence of.
     *
     * @return Task
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function fileExists($fname)
    {
        $test = 'if [[ -f '.$fname.' ]]; then echo "file-exists"; else echo "does-not-exist"; fi';
        return self::exec(['eval', $test])->map(function ($result) use ($test) {
            $out = trim($result[0]);
            if ($out === 'file-exists') {
                return true;
            }

            if ($out === 'does-not-exist') {
                return false;
            }

            throw new \RuntimeException('Received unexpected response from shell while running: '.$test);
        });

    }//end fileExists()


    /**
     * Serial.
     *
     * @param array $tasks An array of tasks to run in serial.
     *
     * Run an array of tasks in serial, one after the other.
     *
     * @return Task
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function serial(array $tasks)
    {
        $reducer = function ($arrayTask, $task) {
            return $arrayTask->chain(function ($arr) use ($task) {
                return $task->map(push($arr));
            });
        };
        return array_reduce($tasks, $reducer, Task::of([]));

    }//end serial()


    /**
     * Parallel.
     *
     * Where possible (i.e. when using the Async/Proc), this will run tasks
     * in parallel.
     *
     * @param array $tasks The tasks to perform.
     *
     * @return Task
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function parallel(array $tasks)
    {
        // Rather than using chain() here, we instead run a loop that will
        // alow any processes to complete. We manually call Proc::runEventLoop()
        // to ensure this happens.
        return new Task(function ($reject, $resolve) use ($tasks) {
            $out       = [];
            $completed = 0;
            $numTasks  = count($tasks);
            foreach ($tasks as $i => $task) {
                $task->fork(
                    function ($err) use (&$completed, $reject) {
                        $completed++;
                        $reject($err);
                    },
                    function ($result) use ($i, &$out, &$completed) {
                        $out[$i] = $result;
                        $completed++;
                    }
                );
            }

            while ($completed < $numTasks) {
                Proc::runEventLoop();
            }

            // Sort keys because PHP will occasionally return an array with correct numeric keys but will iterate over
            // them in the wrong order.
            ksort($out);

            $resolve($out);
        });

    }//end parallel()


}//end class
