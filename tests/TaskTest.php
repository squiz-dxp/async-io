<?php
/**
 * Tests for the Task Class.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\Workplace\Funnelback\Tests
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO\Tests;

require __DIR__.'/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Squiz\AsyncIO\Task;
use Squiz\AsyncIO\Proc;
use Squiz\AsyncIO\Async;

/**
 * Test Task.
 */
class TestTask extends TestCase
{


    /**
     * Calling fork should run the function passed to constructor.
     *
     * @return void
     */
    public function testCallingForkCallsFuncPassedToConstructor()
    {
        $runTest = function () {
            $this->assertTrue(true);
        };

        $noop = function () {
        };

        $tsk = new Task($runTest);
        $tsk->fork($noop, $noop);

    }//end testCallingForkCallsFuncPassedToConstructor()


    /**
     * Map should compose resolve functions.
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     *
     * @return void
     */
    public function testMapShouldComposeResolveFunctions()
    {
        $add1 = function ($x) {
            return ($x.' plus one');
        };

        $resolver = function ($reject, $resolve) {
            try {
                return $resolve('one');
            } catch (Exception $e) {
                return $reject($e);
            }
        };

        $tsk = new Task($resolver);

        $tsk = $tsk->map($add1);

        $tsk->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($val) {
                $this->assertEquals($val, 'one plus one');
            }
        );

    }//end testMapShouldComposeResolveFunctions()


    /**
     * Of should return a task that eventually resolves to input.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testOfResolvesToInput()
    {
        $input    = 'Slartibartfast';
        $expected = $input;
        $tsk      = Task::of($input);
        $tsk->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testOfResolvesToInput()


    /**
     * Chain should chain functions that return tasks.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testChainChainsTasks()
    {
        $input  = Task::of('two');
        $addTwo = function ($str) {
            return Task::of($str.' plus two');
        };

        $expected = 'two plus two';

        $tsk = $input->chain($addTwo);
        $tsk->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testChainChainsTasks()


    /**
     * Join should un-nest two tasks.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testJoinShouldUnnestTasks()
    {
        $expected = 'Arthur Dent';
        $input    = Task::of(Task::of($expected));
        $input->join()->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testJoinShouldUnnestTasks()


    /**
     * Ap should apply function in a task to another task value.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testApAppliesFunctionToTaskValue()
    {
        $plusOne = function ($val) {
            return $val.' plus one';
        };

        $funcTask = Task::of($plusOne);
        $valTask  = Task::of('one');

        $expected = 'one plus one';

        $valTask->ap($funcTask)->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testApAppliesFunctionToTaskValue()


    /**
     * Ap should keep composition rule.
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function testApKeepsCompositionRule()
    {
        // Composition rule: v.ap(u.ap(a.map(f => g => x => f(g(x))))) is equivalent to v.ap(u).ap(a) (composition).
        $composeCurried = function ($f) {
            return function ($g) use ($f) {
                return function ($x) use ($f, $g) {
                    return $f($g($x));
                };
            };
        };

        $plusOne = function ($val) {
            return $val.' plus one';
        };

        $plusTwo = function ($val) {
            return $val.' plus two';
        };

        $v = Task::of('one');
        $u = Task::of($plusOne);
        $a = Task::of($plusTwo);

        $expected = 'one plus one plus two';

        $v->ap($u)->ap($a)->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

        $v->ap($u->ap($a->map($composeCurried)))->fork(
            function ($e) {
                $this->assertTrue(false, 'Error:'.$e);
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testApKeepsCompositionRule()


    /**
     * Test chain() returns chained results.
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testChainReturnsChainedResults()
    {
        $add1 = function ($x) {
            return Task::of($x + 1);
        };

        $task = Task::of(0)->chain($add1)->chain($add1)->chain($add1)->chain($add1);
        $task->fork(
            function () {
                throw new \RuntimeException('Could not run task fork for chained tasks');
            },
            function ($actual) {
                $expected = 4;
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testChainReturnsChainedResults()


    /**
     * Task reject should call rejection side of fork.
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function testTaskRejectCallsRejectionSideOfFork()
    {
        $expected = 'This is a test string';
        $err      = new \RuntimeException($expected);
        $task     = Task::reject($err);
        $task->fork(
            function ($e) use ($expected) {
                $actual = $e->getMessage();
                $this->assertEquals($expected, $actual);
            },
            function ($actual) {
                $this->assertTrue(false, 'Failed to call reject for rejected fork');
            }
        );

        $task = Task::reject($err)->map(function () {
            $this->assertTrue(false, 'Rejected tasks should not call map');
        })->fork(
            function ($e) use ($expected) {
                $actual = $e->getMessage();
                $this->assertEquals($expected, $actual);
            },
            function ($actual) {
                $this->assertTrue(false, 'Failed to call reject for rejected fork');
            }
        );

        $task = Task::reject($err)->chain(function () {
            $this->assertTrue(false, 'Rejected tasks should not call chain');
            return Task::of(false);
        })->fork(
            function ($e) use ($expected) {
                $actual = $e->getMessage();
                $this->assertEquals($expected, $actual);
            },
            function ($actual) {
                $this->assertTrue(false, 'Failed to call reject for rejected fork');
            }
        );

    }//end testTaskRejectCallsRejectionSideOfFork()


    /**
     * Filter tasks should work with chained predicates.
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function testFilterTasksShouldFilterChainedPredicate()
    {
        $predicate = function ($bool) {
            return Task::of(!empty($bool));
        };

        $values = [
            true,
            false,
            true,
            false,
            true,
        ];

        Task::filterByTaskPredicate($predicate, $values)->map(function ($actual) {
            $expected = [true, true, true];
            $this->assertEquals($expected, $actual);
        })->fork(
            function ($e) {
                $this->assertTrue(false, "Failed to run test for filtering tasks\n".$e);
            },
            function ($actual) {
                $this->assertTrue(true);
            }
        );

    }//end testFilterTasksShouldFilterChainedPredicate()


}//end class
