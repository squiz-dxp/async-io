<?php
/**
 * Tests for Async functions.
 *
 * PHP Version 5.4+
 *
 * @package Squiz\Workplace\Funnelback\Tests
 * @author  James Sinclair <jsinclair@squiz.net>
 */
namespace Squiz\AsyncIO\Tests;

require __DIR__.'/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Squiz\AsyncIO\Task;
use Squiz\AsyncIO\Async;
use Squiz\AsyncIO\Proc;

/**
 * Test FS.
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class TestFS extends TestCase
{


    /**
     * ReadFile should return a task.
     *
     * @return void
     */
    public function testReadFileShouldReturnTask()
    {
        $fname    = '/tmp/tmp.txt';
        $expected = 'I’ve got a lovely bunch of coconuts';
        $procStub = function ($cmd, $reject, $resolve) use ($fname, $expected) {
            $this->assertEquals(['cat', $fname], $cmd);
            try {
                $resolve([$expected, '']);
            } catch (Exception $e) {
                $reject($e);
            }
        };
        $fileTask = Async::readFile('/tmp/tmp.txt', $procStub);
        $this->assertInstanceOf('Squiz\AsyncIO\Task', $fileTask);
        $fileTask->fork(
            function ($err) {
                $this->assertFalse(true, "ioReadFile() rejected:\n".var_export($err, true));
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testReadFileShouldReturnTask()


    /**
     * Async exec() should return a task that runs a command.
     *
     * @return void
     */
    public function testAsyncExecReturnsATaskThatRunsCommand()
    {
        $cmd      = ['git', '--version'];
        $expected = "git version 2.18.0\n";
        $procStub = function ($cmd1, $reject, $resolve) use ($cmd, $expected) {
            $this->assertEquals($cmd1, $cmd);
            try {
                $resolve([$expected, '']);
            } catch (Exception $e) {
                $reject($e);
            }
        };
        $execTask = Async::exec($cmd, $procStub);
        $this->assertInstanceOf('Squiz\AsyncIO\Task', $execTask);
        $execTask->fork(
            function ($err) {
                $this->assertFalse(true, "ioExec() rejected:\n".var_export($err, true));
            },
            function ($actual) use ($expected) {
                $this->assertEquals($expected, $actual[0]);
            }
        );

    }//end testAsyncExecReturnsATaskThatRunsCommand()


    /**
     * Writing a file should return a task that writes to a file.
     *
     * @return void
     */
    public function testWriteFileWritesToFile()
    {
        $fname = '/tmp/sometemporaryfile.txt';
        @unlink($fname);

        $output    = [
            'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum',
            'tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas',
            'semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien',
            'ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean',
            'fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.'
        ];
        $outputstr = implode(' ', $output);
        Async::writeFile($fname, $outputstr)->chain(function () use ($fname) {
            return Async::readFile($fname);
        })->fork(
            function ($e) {
                $this->assertTrue(false, 'Failed to write data to file'.$fname.":\n".$e);
            },
            function ($actual) use ($outputstr) {
                $expected = $outputstr;
                $this->assertEquals($expected, $actual);
            }
        );

        // Run the event loop so PHPUnit can see the result before this method finishes.
        Proc::runEventLoop();

    }//end testWriteFileWritesToFile()


    /**
     * File Exists should determine if file exists.
     *
     * @return void
     */
    public function testFileExistsReturnsWhetherFileExists()
    {
        chdir('/tmp');
        @mkdir('existence-tests');
        chdir('/tmp/existence-tests');
        $dirpath = '/tmp/existence-tests';

        $filenames = [
            array('fname' => $dirpath.'/exists.txt', 'shouldExist' => true),
            array('fname' => $dirpath.'/does-not-exist.txt', 'shouldExist' => false),
            array('fname' => $dirpath.'/exists-no-suffix', 'shouldExist' => true),
            array('fname' => $dirpath.'/does-not-exist-no-suffix', 'shouldExist' => false),
        ];

        $runTest = function ($data) {
            $fname       = $data['fname'];
            $shouldExist = $data['shouldExist'];
            @unlink($fname);
            if ($shouldExist) {
                touch($fname);
            }

            return Async::fileExists($fname)->map(function ($actual) use ($shouldExist, $fname) {
                $expected = $shouldExist;
                $msg      = 'Expected existence of file '.$fname.' to be '.var_export($expected, true);
                $this->assertEquals($expected, $actual, $msg);
            });
        };
        Async::serial(array_map($runTest, $filenames))->fork(
            function ($err) {
                $this->assertTrue(false, 'Unable to run tests for checking file existence: '.$err);
            },
            function () {
                $this->assertTrue(true);
            }
        );

        // Run the event loop so PHPUnit can see the result before this method finishes.
        Proc::runEventLoop();

    }//end testFileExistsReturnsWhetherFileExists()


    /**
     * TraverseArray should return Task of Array.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testTraverseArrayShouldReturnArray()
    {
        $tasks = [
            Task::of(1),
            Task::of(2),
            Task::of(3),
            Task::of(4),
            Task::of(5)
        ];
        Async::parallel($tasks)->fork(
            function ($e) {
                throw new \RuntimeException("Could not run task fork for array traversal tasks\n".$e);
            },
            function ($actual) {
                $expected = [1, 2, 3, 4, 5];
                $this->assertEquals($expected, $actual);
            }
        );

    }//end testTraverseArrayShouldReturnArray()


    /**
     * Serial should run tasks in serial.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function testSerialRunsTaskInSerial()
    {
        $tasks = [
            Task::of('a'),
            Task::of('b'),
            Task::of('c'),
            Task::of('d'),
            Task::of('e')
        ];
        Async::serial($tasks)->fork(
            function ($e) {
                throw new \RuntimeException("Could not run task fork for serial traversal tasks\n".$e);
            },
            function ($actual) {
                $expected = ['a', 'b', 'c', 'd', 'e'];
                $this->assertEquals($expected, $actual);
            }
        );

        $trace = function ($msg) {
            return Task::of($msg)->map(function ($msg) {
                echo $msg."\n";
                return $msg;
            })->chain(function ($msg) {
                return Async::exec(['echo', $msg])->map(function ($result) {
                    $msg = trim($result[0]);
                    echo $msg."\n";
                    return $msg;
                });
            });
        };

        $messages = ['alpha', 'beta', 'gamma', 'epsilon', 'theta'];
        $expected = ['alpha', 'alpha', 'beta', 'beta', 'gamma', 'gamma', 'epsilon', 'epsilon', 'theta', 'theta'];
        Async::serial(array_map($trace, $messages))->fork(
            function ($e) {
                throw new \RuntimeException("Could not run task fork for serial traversal tasks\n".$e);
            },
            function ($actual) use ($expected, $messages) {
                $this->assertEquals($messages, $actual);
                $this->expectOutputString(implode("\n", $expected)."\n");
            }
        );

        Proc::runEventLoop();

    }//end testSerialRunsTaskInSerial()


}//end class
